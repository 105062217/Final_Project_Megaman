var Megaman = Megaman || {};

Megaman.Lose = function(game){
    this.game = game;
};

Megaman.Lose.prototype = {
    preload: function() {
        this.load.image('lose_scene', 'assets/lose_scene.png');
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    },
    create: function() {
        this.lose_scene = this.add.image(0, 0, 'lose_scene');
        this.startButton = this.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    },

    update: function(){       
        if(this.startButton.isDown){
            this.state.start('Menu');
        }
    }
};